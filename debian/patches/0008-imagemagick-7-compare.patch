From: Debian Python Team <team+python@tracker.debian.org>
Date: Mon, 11 Nov 2024 17:14:38 +1100
Subject: Rework image comparison test for imagemagick-7

Use an explicit intermediate PNG file with imagemagick-7 to make sure
the tests are comparing images of the same size.
---
 unittests/helpers/utils.py | 60 +++++++++++++++++++++++++++++++++-------------
 1 file changed, 44 insertions(+), 16 deletions(-)

diff --git a/unittests/helpers/utils.py b/unittests/helpers/utils.py
index ad29b07..888671f 100644
--- a/unittests/helpers/utils.py
+++ b/unittests/helpers/utils.py
@@ -9,24 +9,52 @@ from hashlib import md5
 CACHE_LOCATION = Path(__file__).absolute().parent / "compare_cache"
 
 def cmp_img(a: str, b: str) -> float:
-    out = subprocess.run(["compare", "-quiet", "-metric", "MSE",
-        '-subimage-search', '-resize', '200x200', a, b, "NULL:"],
-        stderr=subprocess.PIPE, stdin=subprocess.DEVNULL, check=False)
+    class CompareError(Exception):
+        def __init__(self, message):
+            self.message = message
 
-    # return code 1 is for dissimilar images, but we use our own threshold
-    # since imagemagick is too strict
-    if out.returncode == 2:
-        class CompareError(Exception):
-            def __init__(self, message):
-                self.message = message
-        out = subprocess.run(["compare", "-verbose", "-metric", "MSE",
-            '-subimage-search', '-resize', '200x200', a, b, "NULL:"],
-            stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.DEVNULL,
-            check=False)
-        print(out.stdout.decode())
-        print(out.stderr.decode())
+    def svg2png(f: str, tmpf: str) -> None:
+        out = subprocess.run(
+            [
+                "convert", "-density", "600",
+                "-resize", "1000x1000", "-extent", "1000x1000",
+                f,
+                tmpf,
+            ],
+            stdin=subprocess.DEVNULL,
+            stdout=subprocess.PIPE,
+            stderr=subprocess.PIPE,
+            check=False,
+        )
+        if out.returncode != 0:
+            print(out.stdout.decode())
+            print(out.stderr.decode())
+            raise CompareError(f"Could not work with {a}: {out.stdout.decode()} {out.stderr.decode()}")
 
-        raise CompareError("Compare failed on {}, {}".format(a, b))
+    with TemporaryDirectory() as tmpdir:
+        tmppath = Path(tmpdir)
+        tmp1 = tmppath / "tmp1.png"
+        svg2png (a, tmp1)
+        tmp2 = tmppath / "tmp2.png"
+        svg2png (b, tmp2)
+
+        out = subprocess.run(
+            [
+                "compare", "-quiet", "-metric", "MSE",
+                '-subimage-search',
+                tmp1, tmp2, "NULL:",
+            ],
+            stdin=subprocess.DEVNULL,
+            stderr=subprocess.PIPE,
+            stdout=subprocess.PIPE,
+            check=False,
+        )
+
+    # if out.returncode == 2:
+    #     # exit 1 is for dissimilar images (comparison completely failed)
+    #     print(out.stdout.decode())
+    #     print(out.stderr.decode())
+    #     raise CompareError("Compare failed on {}, {}".format(a, b))
 
     # The result is b"... (diff)"
     output = out.stderr.decode()
